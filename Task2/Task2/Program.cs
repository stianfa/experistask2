﻿using System;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Get name from the user
            Console.WriteLine("Please enter your name:");

            string name = Console.ReadLine();

            // Output the desired string with name, length of name and first letter
            Console.WriteLine("Hello " + name + ", your name is " + name.Length + " characters long and starts with a " + name[0] + ".");
        }
    }
}